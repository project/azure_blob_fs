<?php

namespace Drupal\azure_blob_fs\StreamWrapper;

use Drupal\azure_blob_fs\Service\AzureBlobFsService;
use MicrosoftAzure\Storage\Blob\BlobSharedAccessSignatureHelper;
use MicrosoftAzure\Storage\Common\Internal\Resources;

/**
 * Defines a Drupal stream wrapper class for use with private scheme.
 *
 * Provides support for storing files on the azure file system with the
 * Drupal file interface.
 */
class PrivateAzureBlobFsStream extends AzureBlobFsStream {

  /**
   * The Azure Blob Storage SAS Helper.
   *
   * @var \MicrosoftAzure\Storage\Blob\BlobSharedAccessSignatureHelper
   */
  protected $sasHelper;

  /**
   * The Azure Blob Storage SAS Token.
   *
   * @var string
   */
  protected static $blobStorageSASToken;

  /**
   * Constructs a new AzureBlobFsStream object.
   *
   * We load the sas helper as an additional step here.
   */
  public function __construct() {
    parent::__construct();
    // Load the client.
    $this->loadSasHelper();
  }

  /**
   * {@inheritdoc}
   */
  public function scheme() : string {
    return 'private';
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->t('Private Files (Azure Blob Storage)');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->t('Private files served from an Azure Blob Storage.');
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl(): string {
    // Generate a short lived sas token.
    // We set a 30 minute expiry time here.
    $expiry = new \DateTime;
    $expiry->setTimestamp(time() + (60 * 30));
    $expiry->setTimezone(new \DateTimeZone('UTC'));
    $sas = $this->sasHelper->generateBlobServiceSharedAccessSignatureToken(
      Resources::RESOURCE_TYPE_BLOB,
      static::$container . '/' . file_uri_target($this->uri),
      'r',
      $expiry->format('Y-m-d\TH:i:s\Z'),
      (new \DateTime('now'))->format('Y-m-d\TH:i:s\Z'),
      '',
      'https'
    );

    $blobUrlWithSAS = sprintf(
      '%s%s?%s',
      (string) $this->getClient()->getPsrPrimaryUri(),
      static::$container . '/' . file_uri_target($this->uri),
      $sas
    );

    // Return the external url with the appended sas.
    return $blobUrlWithSAS;
  }

  /**
   * Loads the SAS Helper to our class.
   */
  protected function loadSasHelper(): void {
    // Create the client.
    $this->sasHelper = new BlobSharedAccessSignatureHelper(static::$accountName, static::$accountKey);
  }

}
