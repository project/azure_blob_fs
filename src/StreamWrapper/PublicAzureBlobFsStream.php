<?php

namespace Drupal\azure_blob_fs\StreamWrapper;

/**
 * Defines a Drupal stream wrapper class for use with public scheme.
 *
 * Provides support for storing files on the Azure file system with the
 * Drupal file interface.
 */
class PublicAzureBlobFsStream extends AzureBlobFsStream {

  /**
   * {@inheritdoc}
   */
  public function scheme() : string {
    return 'public';
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->t('Public Files (Azure Blob Storage)');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->t('Public files served from an Azure Blob Storage.');
  }

}
