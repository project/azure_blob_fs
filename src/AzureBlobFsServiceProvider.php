<?php

namespace Drupal\azure_blob_fs;

use Drupal;
use Drupal\azure_blob_fs\PathProcessor\AzureBlobFsPathProcessorImageStyles;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Site\Settings;
use Drupal\azure_blob_fs\StreamWrapper\PrivateAzureBlobFsStream;
use Drupal\azure_blob_fs\StreamWrapper\PublicAzureBlobFsStream;
use Drupal\azure_blob_fs\Asset\AzureBlobFsCssOptimizer;
use Drupal\azure_blob_fs\Constants\Settings as ModuleSettings;
use Drupal\azure_blob_fs\Constants\Config as ModuleConfig;
use Drupal\azure_blob_fs\Constants\Services as ModuleServices;


/**
 * The stream wrapper class.
 *
 * In the docs for this class, anywhere you see "<scheme>", it can mean either
 * "azure_blob" or "public", depending on which stream is currently being serviced.
 */
class AzureBlobFsServiceProvider extends ServiceProviderBase {

  /**
   * Register service definitions.
   *
   * @param ContainerBuilder $container
   *   The ContainerBuilder whose service definitions can be altered.
   */
  public function register(ContainerBuilder $container): void {
    // If the module isn't activated, then there's nothing for us to do here.
    if (!Settings::get(ModuleSettings::ACTIVATED)) {
      return;
    }

    // Perform registrations for remote public FS if enabled.
    if (Settings::get(ModuleSettings::ENABLE_PUBLIC_AZURE_BLOB_FS)) {
      // Get the default local public stream wrapper.
      $corePublicStreamWrapper = $container->getDefinition('stream_wrapper.public');
      $corePublicStreamWrapperClass = $corePublicStreamWrapper->getClass();

      // Register the core public stream wrapper as a separate one.
      // We will preserve it under the name 'local'.
      // We do this because we'll be overwriting the original.
      $container->register('stream_wrapper.local', $corePublicStreamWrapperClass)
        ->addTag('stream_wrapper', ['scheme' => 'local']);
    }

    // Perform registrations for remote private FS if enabled.
    if (Settings::get(ModuleSettings::ENABLE_PRIVATE_AZURE_BLOB_FS)) {
      // If the local private file system exists, we want to keep it.
      // In the alter() function below, we alter the existing one.
      // Otherwise, we simply register ours.
      if ($container->hasDefinition('stream_wrapper.private')) {
        // Get the default local public stream wrapper.
        $corePrivateStreamWrapper = $container->getDefinition('stream_wrapper.private');
        $corePrivateStreamWrapperClass = $corePrivateStreamWrapper->getClass();
        $container->register('stream_wrapper.local_private', $corePrivateStreamWrapperClass)
          ->addTag('stream_wrapper', ['scheme' => 'local_private']);
      }
      else {
        // Get the default local public stream wrapper.
        $container->register('stream_wrapper.private', PrivateAzureBlobFsStream::class)
          ->addTag('stream_wrapper', ['scheme' => 'private']);
      }
    }
  }

  /**
   * Modifies existing service definitions.
   *
   * @param ContainerBuilder $container
   *   The ContainerBuilder whose service definitions can be altered.
   */
  public function alter(ContainerBuilder $container): void {
    // If the module isn't activated, then there's nothing for us to do here.
    if (!Settings::get(ModuleSettings::ACTIVATED)) {
      return;
    }

    // For now, we're doing a wide service override.
    // When the module is enabled, the core file system is replaced.
    // @TODO - In the future, we want to NOT do this and have the Azure file system available alongside the local filesystem.
    // @TODO - Additionally, we will want a PUBLIC AZURE file system and a PRIVATE AZURE file system to differentiate the two.
    // @TODO - Again, we don't really want to decorate/override core service, but instead add the azure file system to make it available for configurations on fields and in the file system site config.
    if (Settings::get(ModuleSettings::ENABLE_PUBLIC_AZURE_BLOB_FS)) {
      // Replace the core public stream wrapper with our Azure wrapper.
      $container->getDefinition('stream_wrapper.public')
        ->setClass(PublicAzureBlobFsStream::class);

      // Replace the core Image Style Path Processor with ours.
      $azurePathPreprocessorImageStylesDefinition = $container->getDefinition(ModuleServices::PATH_PROCESSOR__IMAGE_STYLES);
      $container->getDefinition('path_processor.image_styles')
        ->setClass($azurePathPreprocessorImageStylesDefinition->getClass())
        ->setArguments($azurePathPreprocessorImageStylesDefinition->getArguments());

      // Fix CSS static urls.
      $container->getDefinition('asset.css.optimizer')
        ->setClass(AzureBlobFsCssOptimizer::class);
    }

    // If our private file system is enabled we do actions here.
    // We only alter the core private stream if our local fallback isn't there.
    if (Settings::get(ModuleSettings::ENABLE_PRIVATE_AZURE_BLOB_FS) && !$container->hasDefinition('stream_wrapper.local_private')) {
      // Replace the private stream wrapper with our Azure wrapper.
      $container->getDefinition('stream_wrapper.private')
        ->setClass(PrivateAzureBlobFsStream::class);
    }
  }

}
