<?php

namespace Drupal\azure_blob_fs;

/**
 * Class used to differentiate between known and unknown exception states.
 */
class AzureBlobFsException extends \Exception {}
