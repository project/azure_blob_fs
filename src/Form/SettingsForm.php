<?php

namespace Drupal\azure_blob_fs\Form;

use Drupal\azure_blob_fs\Constants\Config as ModuleConfig;
use Drupal\azure_blob_fs\Constants\Services as ModuleServices;
use Drupal\azure_blob_fs\Constants\Settings as ModuleSettings;
use Drupal\azure_blob_fs\Service\AzureBlobFsServiceInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures module settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Azure Blob FS service injected through DI.
   *
   * @var \Drupal\azure_blob_fs\Service\AzureBlobFsService
   */
  protected $azureBlobFsService;

  /**
   * HarmonizeConfigForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal's Configuration Factory injected through DI.
   * @param \Drupal\azure_blob_fs\Service\AzureBlobFsServiceInterface $azure_blob_fs_service
   *   The Azure Blob FS service injected through DI.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AzureBlobFsServiceInterface $azure_blob_fs_service) {
    parent::__construct($config_factory);
    $this->azureBlobFsService = $azure_blob_fs_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('config.factory'),
      $container->get(ModuleServices::AZURE_BLOB_FS)
    );
  }

  /**
   * Provide a constant for the Form ID of this form.
   *
   * @var string
   */
  public const FORM_ID = 'azure_blob_fs_settings_form';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      ModuleConfig::EDITABLE_CONFIG_KEY__SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL): array {
    $config = $this->config(ModuleConfig::EDITABLE_CONFIG_KEY__SETTINGS)->getRawData();
    $settings = $this->azureBlobFsService->getSettings();
    $overridden = FALSE;

    $form[ModuleConfig::SETTINGS__ACTIVATED] = [
      '#type'          => 'checkbox',
      '#title'         => t('Enable Module Functionality'),
      '#description'   => t(
        'Check this box to enable module functionality. Use this as a global toggle for all module functionality.'
      ),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__ACTIVATED]) ? $config[ModuleConfig::SETTINGS__ACTIVATED] : 0,
      '#weight'        => -15,
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::ACTIVATED])) {
      $overridden = TRUE;
      $form['activated']['#default_value'] = $settings[ModuleSettings::ACTIVATED] ? 1 : 0;
      $form['activated']['#disabled'] = TRUE;
      $form['activated']['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    $form['tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => t('Settings'),
    ];

    $form['tabs']['account'] = [
      '#type' => 'details',
      '#title' => t('Account Credentials'),
      '#group' => 'tabs',
    ];

    // Blob Storage URL.
    $form['tabs']['account'][ModuleConfig::SETTINGS__URL] = [
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description'   => t('Enter the URL of the blob storage.'),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__URL]) ? $config[ModuleConfig::SETTINGS__URL] : '',
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::URL])) {
      $overridden = TRUE;
      $form['tabs']['account'][ModuleConfig::SETTINGS__URL]['#default_value'] = $settings[ModuleSettings::URL];
      $form['tabs']['account'][ModuleConfig::SETTINGS__URL]['#disabled'] = TRUE;
      $form['tabs']['account'][ModuleConfig::SETTINGS__URL]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    // Account Name.
    $form['tabs']['account'][ModuleConfig::SETTINGS__ACCOUNT_NAME] = [
      '#type' => 'textfield',
      '#title' => t('Account Name'),
      '#description'   => t('Enter the Account Name.'),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__ACCOUNT_NAME]) ? $config[ModuleConfig::SETTINGS__ACCOUNT_NAME] : '',
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::ACCOUNT_NAME])) {
      $overridden = TRUE;
      $form['tabs']['account'][ModuleConfig::SETTINGS__ACCOUNT_NAME]['#default_value'] = $settings[ModuleSettings::ACCOUNT_NAME];
      $form['tabs']['account'][ModuleConfig::SETTINGS__ACCOUNT_NAME]['#disabled'] = TRUE;
      $form['tabs']['account'][ModuleConfig::SETTINGS__ACCOUNT_NAME]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    // Account Key.
    $form['tabs']['account'][ModuleConfig::SETTINGS__ACCOUNT_KEY] = [
      '#type' => 'textfield',
      '#title' => t('Account Key'),
      '#description'   => t('Enter the Account Key.'),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__ACCOUNT_KEY]) ? $config[ModuleConfig::SETTINGS__ACCOUNT_KEY] : '',    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::ACCOUNT_KEY])) {
      $overridden = TRUE;
      $form['tabs']['account'][ModuleConfig::SETTINGS__ACCOUNT_KEY]['#default_value'] = $settings[ModuleSettings::ACCOUNT_KEY];
      $form['tabs']['account'][ModuleConfig::SETTINGS__ACCOUNT_KEY]['#disabled'] = TRUE;
      $form['tabs']['account'][ModuleConfig::SETTINGS__ACCOUNT_KEY]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    /**
     * Public File System Settings.
     */
    $form['tabs']['public'] = [
      '#type' => 'details',
      '#title' => t('Public File System'),
      '#group' => 'tabs',
    ];

    $form['tabs']['public'][ModuleConfig::SETTINGS__ENABLE_PUBLIC_AZURE_BLOB_FS] = [
      '#type'          => 'checkbox',
      '#title'         => t('Enable Public Azure Blob File System'),
      '#description'   => t(
        'Check this box to enable the use of a remote public file system connected to an Azure Blob storage.'
      ),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__ENABLE_PUBLIC_AZURE_BLOB_FS]) ? $config[ModuleConfig::SETTINGS__ENABLE_PUBLIC_AZURE_BLOB_FS] : 0,
      '#weight'        => -15,
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::ENABLE_PUBLIC_AZURE_BLOB_FS])) {
      $overridden = TRUE;
      $form['tabs']['public'][ModuleConfig::SETTINGS__ENABLE_PUBLIC_AZURE_BLOB_FS]['#default_value'] = $settings[ModuleSettings::ENABLE_PUBLIC_AZURE_BLOB_FS] ? 1 : 0;
      $form['tabs']['public'][ModuleConfig::SETTINGS__ENABLE_PUBLIC_AZURE_BLOB_FS]['#disabled'] = TRUE;
      $form['tabs']['public'][ModuleConfig::SETTINGS__ENABLE_PUBLIC_AZURE_BLOB_FS]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    // Public SAS Token.
    $form['tabs']['public'][ModuleConfig::SETTINGS__PUBLIC_SAS_TOKEN] = [
      '#type' => 'textfield',
      '#title' => t('Public SAS Token'),
      '#description'   => t('Enter the SAS Token that will be used to connect to the public container.'),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__PUBLIC_SAS_TOKEN]) ? $config[ModuleConfig::SETTINGS__PUBLIC_SAS_TOKEN] : '',
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::PUBLIC_SAS_TOKEN])) {
      $overridden = TRUE;
      $form['tabs']['public'][ModuleConfig::SETTINGS__PUBLIC_SAS_TOKEN]['#default_value'] = $settings[ModuleSettings::PUBLIC_SAS_TOKEN];
      $form['tabs']['public'][ModuleConfig::SETTINGS__PUBLIC_SAS_TOKEN]['#disabled'] = TRUE;
      $form['tabs']['public'][ModuleConfig::SETTINGS__PUBLIC_SAS_TOKEN]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    // Public Container Name.
    $form['tabs']['public'][ModuleConfig::SETTINGS__PUBLIC_CONTAINER_NAME] = [
      '#type' => 'textfield',
      '#title' => t('Public Container Name'),
      '#description'   => t('Enter the name of the container that will be used for the public file system.'),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__PUBLIC_CONTAINER_NAME]) ? $config[ModuleConfig::SETTINGS__PUBLIC_CONTAINER_NAME] : '',
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::PUBLIC_CONTAINER_NAME])) {
      $overridden = TRUE;
      $form['tabs']['public'][ModuleConfig::SETTINGS__PUBLIC_CONTAINER_NAME]['#default_value'] = $settings[ModuleSettings::PUBLIC_CONTAINER_NAME];
      $form['tabs']['public'][ModuleConfig::SETTINGS__PUBLIC_CONTAINER_NAME]['#disabled'] = TRUE;
      $form['tabs']['public'][ModuleConfig::SETTINGS__PUBLIC_CONTAINER_NAME]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    /**
     * Private File System Settings.
     */
    $form['tabs']['private'] = [
      '#type' => 'details',
      '#title' => t('Private File System'),
      '#group' => 'tabs',
    ];

    $form['tabs']['private'][ModuleConfig::SETTINGS__ENABLE_PRIVATE_AZURE_BLOB_FS] = [
      '#type'          => 'checkbox',
      '#title'         => t('Enable Private Azure Blob File System'),
      '#description'   => t(
        'Check this box to enable the use of a remote private file system connected to an Azure Blob storage.'
      ),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__ENABLE_PRIVATE_AZURE_BLOB_FS]) ? $config[ModuleConfig::SETTINGS__ENABLE_PRIVATE_AZURE_BLOB_FS] : 0,
      '#weight'        => -15,
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::ENABLE_PRIVATE_AZURE_BLOB_FS])) {
      $overridden = TRUE;
      $form['tabs']['private'][ModuleConfig::SETTINGS__ENABLE_PRIVATE_AZURE_BLOB_FS]['#default_value'] = $settings[ModuleSettings::ENABLE_PRIVATE_AZURE_BLOB_FS] ? 1 : 0;
      $form['tabs']['private'][ModuleConfig::SETTINGS__ENABLE_PRIVATE_AZURE_BLOB_FS]['#disabled'] = TRUE;
      $form['tabs']['private'][ModuleConfig::SETTINGS__ENABLE_PRIVATE_AZURE_BLOB_FS]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    // Private SAS Token.
    $form['tabs']['private'][ModuleConfig::SETTINGS__PRIVATE_SAS_TOKEN] = [
      '#type' => 'textfield',
      '#title' => t('Private SAS Token'),
      '#description'   => t('Enter the SAS Token that will be used to connect to the public container.'),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__PRIVATE_SAS_TOKEN]) ? $config[ModuleConfig::SETTINGS__PRIVATE_SAS_TOKEN] : '',
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::PRIVATE_SAS_TOKEN])) {
      $overridden = TRUE;
      $form['tabs']['private'][ModuleConfig::SETTINGS__PRIVATE_SAS_TOKEN]['#default_value'] = $settings[ModuleSettings::PRIVATE_SAS_TOKEN];
      $form['tabs']['private'][ModuleConfig::SETTINGS__PRIVATE_SAS_TOKEN]['#disabled'] = TRUE;
      $form['tabs']['private'][ModuleConfig::SETTINGS__PRIVATE_SAS_TOKEN]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    // Private Container Name.
    $form['tabs']['private'][ModuleConfig::SETTINGS__PRIVATE_CONTAINER_NAME] = [
      '#type' => 'textfield',
      '#title' => t('Private Container Name'),
      '#description'   => t('Enter the name of the container that will be used for the private file system.'),
      '#default_value' => !empty($config[ModuleConfig::SETTINGS__PRIVATE_CONTAINER_NAME]) ? $config[ModuleConfig::SETTINGS__PRIVATE_CONTAINER_NAME] : '',
    ];

    // If the settings are set in the settings.php file, disable the field.
    if(isset($settings[ModuleSettings::PRIVATE_CONTAINER_NAME])) {
      $overridden = TRUE;
      $form['tabs']['private'][ModuleConfig::SETTINGS__PRIVATE_CONTAINER_NAME]['#default_value'] = $settings[ModuleSettings::PRIVATE_CONTAINER_NAME];
      $form['tabs']['private'][ModuleConfig::SETTINGS__PRIVATE_CONTAINER_NAME]['#disabled'] = TRUE;
      $form['tabs']['private'][ModuleConfig::SETTINGS__PRIVATE_CONTAINER_NAME]['#description'] .= '<br><strong>This field is currently being overwritten by a declaration in your settings.php file. All settings in settings.php take precedence. If you want to use this config, unset the line in your settings.php file.</strong>';
    }

    // Set a warning message if anything is overridden in the settings.php file.
    if ($overridden) {
      $this->messenger()->addWarning('Some configuration is being overridden by code written in your settings.php file. All fields in question will be identified here and disabled since anything set in your settings.php takes precedence over site configurations.');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->config('azure_blob_fs.settings')
      ->set('sas_token', $values['sas_token'])
      ->set('blob_storage_url', $values['blob_storage_url'])
      ->set('container_name', $values['container_name'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
