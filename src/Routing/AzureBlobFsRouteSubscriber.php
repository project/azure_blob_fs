<?php
namespace Drupal\azure_blob_fs\Routing;

use Drupal\azure_blob_fs\Service\AzureBlobFsService;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class AzureBlobFsRouteSubscriber extends RouteSubscriberBase {

  /**
   * The Azure Blob File System main service.
   *
   * @var \Drupal\azure_blob_fs\Service\AzureBlobFsService
   */
  protected $azureBlobFsService;

  /**
   * Constructs a new AzureBlobFsImageStyleRoutes object.
   *
   * @param \Drupal\azure_blob_fs\Service\AzureBlobFsService $azure_blob_fs_service
   *   The Azure Blob File System main service.
   */
  public function __construct(AzureBlobFsService $azure_blob_fs_service) {
    $this->azureBlobFsService = $azure_blob_fs_service;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // Only do the rest if the module is enabled and wired.
    if ($this->azureBlobFsService->publicAzureFileSystemIsEnabled() && $route = $collection->get('image.style_public')) {
      $collection->remove('image.style_public');
    }
  }

}
