<?php

namespace Drupal\azure_blob_fs\Routing;

use Drupal\azure_blob_fs\Service\AzureBlobFsService;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class AzureBlobFsImageStyleRoutes implements ContainerInjectionInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The Azure Blob File System main service.
   *
   * @var \Drupal\azure_blob_fs\Service\AzureBlobFsService
   */
  protected $azureBlobFsService;

  /**
   * Constructs a new AzureBlobFsImageStyleRoutes object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager service.
   * @param \Drupal\azure_blob_fs\Service\AzureBlobFsService $azure_blob_fs_service
   *   The Azure Blob File System main service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, StreamWrapperManagerInterface $stream_wrapper_manager, AzureBlobFsService $azure_blob_fs_service) {
    $this->moduleHandler = $module_handler;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->azureBlobFsService = $azure_blob_fs_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /* @noinspection PhpParamsInspection */
    return new static(
      $container->get('module_handler'),
      $container->get('stream_wrapper_manager'),
      $container->get('azure_blob_fs')
    );
  }

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes(): array {
    $routes = [];

    // Only do the rest if the module is enabled and wired.
    if ($this->azureBlobFsService->publicAzureFileSystemIsEnabled()) {
      // Generate image derivatives of publicly available files. If clean URLs are
      // disabled image derivatives will always be served through the menu system.
      // If clean URLs are enabled and the image derivative already exists, PHP
      // will be bypassed.
      /* @noinspection PhpUndefinedMethodInspection */
      $directory_path = $this->streamWrapperManager->getViaScheme('local')->getDirectoryPath();

      $routes['azure_blob_fs.image_styles'] = new Route(
        '/' . $directory_path . '/styles/{image_style}/{scheme}',
        [
          '_controller' => 'Drupal\azure_blob_fs\Controller\AzureBlobFsImageStyleDownloadController::deliver',
        ],
        [
          '_access' => 'TRUE',
        ]
      );

      // Make our route support redirects.
      /* @see \Drupal\redirect\Routing\RouteSubscriber::alterRoutes() */
      if ($this->moduleHandler->moduleExists('redirect')) {
        $routes['azure_blob_fs.image_styles']->setDefault('_disable_route_normalizer', TRUE);
      }
    }

    return $routes;
  }

}
