<?php

namespace Drupal\azure_blob_fs\Service;

/**
 * Provides an interface for the Azure Blob File System main service.
 */
interface AzureBlobFsServiceInterface {

  /**
   * Get module settings that are set in settings.php.
   *
   * @return array
   *   Array containing settings.
   */
  public function getSettings(): array;

  /**
   * Get module configurations that are stored in the database.
   *
   * @param string $key
   *   Optional configuration key to return from the configurations.
   *
   * @return array|string|null
   *   Array containing configurations.
   */
  public function getConfig(string $key);

  /**
   * Validates that the module's functionality is activated.
   *
   * @return bool
   *   Returns TRUE if the public file system is enabled and configured.
   */
  public function activated(): ?bool;

  /**
   * Validates that the public file system is enabled and configured.
   *
   * @return bool
   *   Returns TRUE if the public file system is enabled and configured.
   */
  public function publicAzureFileSystemIsEnabled(): ?bool;

  /**
   * Validates that the private file system is enabled and configured.
   *
   * @return bool
   *   Returns TRUE if the private file system is enabled and configured.
   */
  public function privateAzureFileSystemIsEnabled(): ?bool;

  /**
   * Get configured activation flag.
   *
   * This will attempt to obtain this from site settings first, then config.
   *
   * @return bool
   *  Returns TRUE if module functionality is activated, FALSE otherwise.
   */
  public function getConfiguredActiveFlag(): ?bool;

  /**
   * Get configured account name.
   *
   * This will attempt to obtain this from site settings first, then config.
   *
   * @return string
   *  Returns account name if set, empty otherwise.
   */
  public function getConfiguredAccountName(): ?string;

  /**
   * Get configured account key.
   *
   * This will attempt to obtain this from site settings first, then config.
   *
   * @return string
   *  Returns account key if set, empty otherwise.
   */
  public function getConfiguredAccountKey(): ?string;

  /**
   * Get configured blob storage URL.
   *
   * This will attempt to obtain this from site settings first, then config.
   *
   * @return string
   *  Returns url if set, empty otherwise.
   */
  public function getConfiguredUrl(): ?string;

  /**
   * Get configured enabling flag for a given file scheme.
   *
   * This will attempt to obtain this from site settings first, then config.
   *
   * There should be values for either the 'public' or 'private' scheme.
   *
   * @param string $scheme
   *   The scheme to fetch the enabling flag for. 'public' or 'private'.
   *
   * @return bool
   *  Returns flag if set, empty otherwise.
   */
  public function getConfiguredEnablingFlag(string $scheme): ?bool;

  /**
   * Get configured SAS Token for a given file scheme.
   *
   * This will attempt to obtain this from site settings first, then config.
   *
   * There should be values for either the 'public' or 'private' scheme.
   *
   * @param string $scheme
   *   The scheme to fetch the SAS token for. 'public' or 'private'.
   *
   * @return string
   *  Returns SAS token if set, empty otherwise.
   */
  public function getConfiguredSasToken(string $scheme): ?string;

  /**
   * Get configured container name for a given file scheme.
   *
   * This will attempt to obtain this from site settings first, then config.
   *
   * There should be values for either the 'public' or 'private' scheme.
   *
   * @param string $scheme
   *   The scheme to fetch the container name for. 'public' or 'private'.
   *
   * @return string
   *  Returns container name if set, empty otherwise.
   */
  public function getConfiguredContainerName(string $scheme): ?string;

}
