<?php

namespace Drupal\azure_blob_fs\Service;

use Drupal\azure_blob_fs\Constants\Config as ModuleConfig;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Site\Settings;
use Drupal\azure_blob_fs\Constants\Settings as ModuleSettings;

/**
 * Defines a service for the Azure Blob File System module.
 */
class AzureBlobFsService implements AzureBlobFsServiceInterface {

  /**
   * Static variable to store module settings.
   *
   * @var array
   */
  protected static $settings;

  /**
   * Static variable to store the module configurations.
   *
   * @var array
   */
  protected static $config;

  /**
   * Static variable to store whether or not the module is activated.
   *
   * @var bool
   */
  protected static $activated;

  /**
   * Static variable to store public azure FS activation flag.
   *
   * @var bool
   */
  protected static $publicAzureFileSystemIsEnabled;

  /**
   * Static variable to store private azure FS activation flag.
   *
   * @var bool
   */
  protected static $privateAzureFileSystemIsEnabled;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs an AzureBlobFsService object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory object.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
    $this->getSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    // If the static variable is set, we can return it.
    if (self::$settings !== NULL) {
      return self::$settings;
    }

    self::$settings = [
      ModuleSettings::ACTIVATED => Settings::get(ModuleSettings::ACTIVATED),
      ModuleSettings::ACCOUNT_NAME => Settings::get(ModuleSettings::ACCOUNT_NAME),
      ModuleSettings::ACCOUNT_KEY => Settings::get(ModuleSettings::ACCOUNT_KEY),
      ModuleSettings::URL => Settings::get(ModuleSettings::URL),
      ModuleSettings::ENABLE_PUBLIC_AZURE_BLOB_FS => Settings::get(ModuleSettings::ENABLE_PUBLIC_AZURE_BLOB_FS),
      ModuleSettings::PUBLIC_SAS_TOKEN => Settings::get(ModuleSettings::PUBLIC_SAS_TOKEN),
      ModuleSettings::PUBLIC_CONTAINER_NAME => Settings::get(ModuleSettings::PUBLIC_CONTAINER_NAME),
      ModuleSettings::ENABLE_PRIVATE_AZURE_BLOB_FS => Settings::get(ModuleSettings::ENABLE_PRIVATE_AZURE_BLOB_FS),
      ModuleSettings::PRIVATE_SAS_TOKEN => Settings::get(ModuleSettings::PRIVATE_SAS_TOKEN),
      ModuleSettings::PRIVATE_CONTAINER_NAME => Settings::get(ModuleSettings::PRIVATE_CONTAINER_NAME),
    ];

    return self::$settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(string $key) {
    // If the static variable is set, we can return it.
    if (self::$config === NULL) {
      self::$config = $this->configFactory->get(ModuleConfig::EDITABLE_CONFIG_KEY__SETTINGS)->getRawData() ?? [];
    }

    return isset(self::$config[$key]) ? self::$config[$key] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function activated(): ?bool {
    // If the static variable is set, we can return it.
    if (self::$activated !== NULL) {
      return self::$activated;
    }

    // We only consider the global activation settings/config flag here.
    self::$activated = Settings::get(ModuleSettings::ACTIVATED);
    return self::$activated;
  }

  /**
   * {@inheritdoc}
   */
  public function publicAzureFileSystemIsEnabled(): ?bool {
    // If the static variable is set, we can return it.
    if (self::$publicAzureFileSystemIsEnabled !== NULL) {
      return self::$publicAzureFileSystemIsEnabled;
    }

    // Get needed settings.
    $publicFsEnabled = Settings::get(ModuleSettings::ENABLE_PUBLIC_AZURE_BLOB_FS);
    $url = Settings::get(ModuleSettings::URL);
    $publicSasToken = Settings::get(ModuleSettings::PUBLIC_SAS_TOKEN);
    $publicContainerName = Settings::get(ModuleSettings::PUBLIC_CONTAINER_NAME);

    // We only consider the global activation settings/config flag here.
    self::$publicAzureFileSystemIsEnabled = ($this->activated() && $publicFsEnabled && $url && $publicSasToken && $publicContainerName);
    return self::$publicAzureFileSystemIsEnabled;
  }

  /**
   * {@inheritdoc}
   */
  public function privateAzureFileSystemIsEnabled(): ?bool {
    // If the static variable is set, we can return it.
    if (self::$privateAzureFileSystemIsEnabled !== NULL) {
      return self::$privateAzureFileSystemIsEnabled;
    }

    // Get needed settings.
    $privateFsEnabled = Settings::get(ModuleSettings::ENABLE_PRIVATE_AZURE_BLOB_FS);
    $url = Settings::get(ModuleSettings::URL);
    $privateSasToken = Settings::get(ModuleSettings::PRIVATE_SAS_TOKEN);
    $privateContainerName = Settings::get(ModuleSettings::PRIVATE_CONTAINER_NAME);

    // We only consider the global activation settings/config flag here.
    self::$privateAzureFileSystemIsEnabled = ($this->activated() && $privateFsEnabled && $url && $privateSasToken && $privateContainerName);
    return self::$privateAzureFileSystemIsEnabled;
  }


  /**
   * {@inheritdoc}
   */
  public function getConfiguredActiveFlag() : ?bool {
    return self::$settings[ModuleSettings::ACTIVATED] ?? $this->getConfig(ModuleConfig::SETTINGS__ACTIVATED);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredAccountName() : ?string {
    return self::$settings[ModuleSettings::ACCOUNT_NAME] ?? $this->getConfig(ModuleConfig::SETTINGS__ACCOUNT_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredAccountKey() : ?string {
    return self::$settings[ModuleSettings::ACCOUNT_KEY] ?? $this->getConfig(ModuleConfig::SETTINGS__ACCOUNT_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredUrl() : ?string {
    return self::$settings[ModuleSettings::URL] ?? $this->getConfig(ModuleConfig::SETTINGS__URL);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredEnablingFlag(string $scheme) : ?bool {
    switch ($scheme) {
      case 'public':
        return self::$settings[ModuleSettings::ENABLE_PUBLIC_AZURE_BLOB_FS] ??$this->getConfig(ModuleConfig::SETTINGS__ENABLE_PUBLIC_AZURE_BLOB_FS);
        break;
      case 'private':
        return self::$settings[ModuleSettings::ENABLE_PRIVATE_AZURE_BLOB_FS] ?? $this->getConfig(ModuleConfig::SETTINGS__ENABLE_PRIVATE_AZURE_BLOB_FS);
        break;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredSasToken(string $scheme) : ?string {
    switch ($scheme) {
      case 'public':
        return self::$settings[ModuleSettings::PUBLIC_SAS_TOKEN] ?? $this->getConfig(ModuleConfig::SETTINGS__PUBLIC_SAS_TOKEN);
        break;
      case 'private':
        return self::$settings[ModuleSettings::PRIVATE_SAS_TOKEN] ?? $this->getConfig(ModuleConfig::SETTINGS__PRIVATE_SAS_TOKEN);
        break;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredContainerName(string $scheme) : ?string {
    switch ($scheme) {
      case 'public':
        return self::$settings[ModuleSettings::PUBLIC_CONTAINER_NAME] ?? $this->getConfig(ModuleConfig::SETTINGS__PUBLIC_CONTAINER_NAME);
        break;
      case 'private':
        return self::$settings[ModuleSettings::PRIVATE_CONTAINER_NAME] ?? $this->getConfig(ModuleConfig::SETTINGS__PRIVATE_CONTAINER_NAME);
        break;
    }

    return NULL;
  }

}
