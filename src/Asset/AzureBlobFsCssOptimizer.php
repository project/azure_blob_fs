<?php

namespace Drupal\azure_blob_fs\Asset;

use Drupal\Core\Asset\CssOptimizer;

/**
 * Optimizes a CSS asset.
 *
 * I'll be honest, I took this from the Amazon S3 module. Probably saved me
 * some headaches.
 */
class AzureBlobFsCssOptimizer extends CssOptimizer {

  /**
   * Return absolute urls to access static files that they aren't in the Azure blob.
   *
   * @param array $matches
   *   An array of matches by a preg_replace_callback() call that scans for
   *   url() references in CSS files, except for external or absolute ones.
   *
   * @param string $base_path
   *
   * @return string
   *   The file path.
   */
  public function rewriteFileURI($matches, $base_path = NULL): string {
    // Prefix with base and remove '../' segments where possible.
    $path = $base_path
      ? $base_path . $matches[1]
      : $this->rewriteFileURIBasePath . $matches[1];

    $last = '';
    while ($path !== $last) {
      $last = $path;
      $path = preg_replace('`(^|/)(?!\.\./)([^/]+)/\.\./`', '$1', $path);
    }
    return 'url(' . file_create_url($path) . ')';
  }

}
