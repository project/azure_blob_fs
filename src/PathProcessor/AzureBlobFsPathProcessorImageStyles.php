<?php

namespace Drupal\azure_blob_fs\PathProcessor;

use Drupal\azure_blob_fs\Service\AzureBlobFsService;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a path processor to rewrite image styles URLs.
 *
 * As the route system does not allow an arbitrary amount of parameters, convert
 * the file path to a query parameter on the request.
 *
 * This processor handles Azure public image style callbacks in order to allow
 * the webserver to serve these files with dynamic args the route is registered
 * under the '/azure/files/styles' prefix and changes internally to pass
 * validation and move the file to query parameter. This file will be processed
 * in AzureBlobFsImageStyleDownloadController::deliver().
 *
 * Private files use the normal private file workflow.
 *
 * @see \Drupal\azure_blob_fs\Controller\AzureBlobFsImageStyleDownloadController::deliver()
 * @see \Drupal\image\Controller\ImageStyleDownloadController::deliver()
 * @see \Drupal\image\PathProcessor\PathProcessorImageStyles::processInbound()
 */
class AzureBlobFsPathProcessorImageStyles implements InboundPathProcessorInterface {

  /**
   * The stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The module's main service.
   *
   * @var \Drupal\azure_blob_fs\Service\AzureBlobFsService
   */
  protected $azureBlobFsService;

  /**
   * Constructs a new PathProcessorImageStyles object.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager service.
   * @param \Drupal\azure_blob_fs\Service\AzureBlobFsService $azure_blob_fs_service
   *   Azure Blob Fs main service.
   */
  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager, AzureBlobFsService $azure_blob_fs_service) {
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->azureBlobFsService = $azure_blob_fs_service;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    // If the module isn't wired, we should return the path.
    if (!$this->azureBlobFsService->publicAzureFileSystemIsEnabled()) {
      return $path;
    }

    /* @noinspection PhpUndefinedMethodInspection */
    $directory_path = $this->streamWrapperManager->getViaScheme('local')->getDirectoryPath();
    if (strpos($path, '/' . $directory_path . '/styles/') === 0) {
      $path_prefix = '/' . $directory_path . '/styles/';
    }
    // Check if the string '/system/files/styles/' exists inside the path,
    // that means we have a case of private file's image style.
    elseif (strpos($path, '/system/files/styles/') !== FALSE) {
      $path_prefix = '/system/files/styles/';
      $path = substr($path, strpos($path, $path_prefix), \strlen($path));
    }
    else {
      return $path;
    }

    // Strip out path prefix.
    $rest = preg_replace('|^' . preg_quote($path_prefix, '|') . '|', '', $path);

    // Get the image style, scheme and path.
    if (substr_count($rest, '/') >= 2) {
      [$image_style, $scheme, $file] = explode('/', $rest, 3);

      // Set the file as query parameter.
      $request->query->set('file', $file);

      return $path_prefix . $image_style . '/' . $scheme;
    }

    return $path;
  }

}
