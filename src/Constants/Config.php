<?php

namespace Drupal\azure_blob_fs\Constants;

/**
 * Provide a class to store constants related to module configurations.
 *
 * @package Drupal\azure_blob_fs\Constants
 */
final class Config {

  /**
   * Store a constant for the editable config for module settings.
   *
   * @var string
   */
  public const EDITABLE_CONFIG_KEY__SETTINGS = 'azure_blob_fs.settings';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__ACTIVATED = 'activated';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__ACCOUNT_NAME = 'account_name';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__ACCOUNT_KEY = 'account_key';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__URL = 'url';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__ENABLE_PUBLIC_AZURE_BLOB_FS = 'enable_public_azure_blob_fs';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__PUBLIC_SAS_TOKEN = 'public_sas_token';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__PUBLIC_CONTAINER_NAME = 'public_container_name';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__ENABLE_PRIVATE_AZURE_BLOB_FS = 'enable_private_azure_blob_fs';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__PRIVATE_SAS_TOKEN = 'private_sas_token';

  /**
   * Store a constant for a configuration element in the settings form.
   *
   * @var string
   */
  public const SETTINGS__PRIVATE_CONTAINER_NAME = 'private_container_name';

}
