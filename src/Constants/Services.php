<?php

namespace Drupal\azure_blob_fs\Constants;

/**
 * Provide a class to store constants related to module services.
 *
 * @package Drupal\azure_blob_fs\Constants
 */
final class Services {

  /**
   * Store a constant for a service declared by this module.
   *
   * @var string
   */
  public const AZURE_BLOB_FS = 'azure_blob_fs';

  /**
   * Store a constant for a service declared by this module.
   *
   * @var string
   */
  public const PATH_PROCESSOR__IMAGE_STYLES = 'azure_blob_fs.path_processor.image_styles';

}
