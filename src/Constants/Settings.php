<?php

namespace Drupal\azure_blob_fs\Constants;

/**
 * Provide a class to store constants related to module settings.
 *
 * @package Drupal\azure_blob_fs\Constants
 */
final class Settings {

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const ACTIVATED = 'azure_blob_fs.activated';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const ACCOUNT_NAME = 'azure_blob_fs.account_name';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const ACCOUNT_KEY = 'azure_blob_fs.account_key';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const URL = 'azure_blob_fs.url';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const ENABLE_PUBLIC_AZURE_BLOB_FS = 'azure_blob_fs.enable_public_azure_blob_fs';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const PUBLIC_SAS_TOKEN = 'azure_blob_fs.public_sas_token';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const PUBLIC_CONTAINER_NAME = 'azure_blob_fs.public_container_name';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const ENABLE_PRIVATE_AZURE_BLOB_FS = 'azure_blob_fs.enable_private_azure_blob_fs';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const PRIVATE_SAS_TOKEN = 'azure_blob_fs.private_sas_token';

  /**
   * Store a constant for a specific module setting.
   *
   * @var string
   */
  public const PRIVATE_CONTAINER_NAME = 'azure_blob_fs.private_container_name';

}
