<?php

namespace Drupal\azure_blob_fs\File;

use Drupal\azure_blob_fs\Service\AzureBlobFsService;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides a FileSystem service decorator for the Azure Blob File System.
 */
class AzureBlobFsFileSystem extends FileSystem {

  /**
   * Without this, the Media Library cries and properly s!%?s itself.
   * I'm not quite sure what the true issue was, but it was super weird.
   * It SEEMS like this class is serialized at some point in the flow of file
   * upload ONLY when using the media library.
   *
   * @TODO - Sherlock holmes this bad boy.
   */
  use DependencySerializationTrait;

  /**
   * Original service object.
   *
   * @var \Drupal\Core\File\FileSystem $file_system
   */
  protected $fileSystem;

  /**
   * The module's main service.
   *
   * @var \Drupal\azure_blob_fs\Service\AzureBlobFsService
   */
  protected $azureBlobFsService;

  /**
   * Constructs a new FileSystem to decorate the original.
   *
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The original FileSystem core service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\Site\Settings $settings
   *   The site settings.
   * @param \Psr\Log\LoggerInterface $logger
   *   The file logger channel.
   * @param \Drupal\azure_blob_fs\Service\AzureBlobFsService $azureBlobFsService
   *   Azure Blob Fs main service.
   */
  public function __construct(FileSystem $file_system, StreamWrapperManagerInterface $stream_wrapper_manager, Settings $settings, LoggerInterface $logger, AzureBlobFsService $azureBlobFsService) {
    $this->fileSystem = $file_system;
    $this->azureBlobFsService = $azureBlobFsService;
    parent::__construct($stream_wrapper_manager, $settings, $logger);
  }

  /**
   * I'm just fed up at this point.
   *
   * This is copy pasted from the core file system, but with a minor change.
   *
   * We don't want to append the basename of the source file to the destination.
   *
   * @see \Drupal\Core\File\FileSystem
   *
   * {@inheritdoc}
   */
  protected function prepareDestination($source, &$destination, $replace): void {
    // First we get the scheme.
    $scheme = $this->uriScheme($destination);

    // Depending on the scheme, we do different checks.
    switch ($scheme) {
      case 'public':
        // If our connection is wired, just return.
        if ($this->azureBlobFsService->publicAzureFileSystemIsEnabled()) {
          $this->determineRealDestination($source, $destination);
          return;
        }
        break;

      case 'private':
        // If our connection is privately wired, just return.
        if ($this->azureBlobFsService->privateAzureFileSystemIsEnabled()) {
          $this->determineRealDestination($source, $destination);
          return;
        }
        break;
    }


    // Otherwise, just do the default parent behavior.
    parent::prepareDestination($source, $destination, $replace);
  }

  /**
   * In Azure Blob Storage, directories don't exist and don't need to be
   * prepared.
   *
   * We want this to return true regardless.
   *
   * {@inheritdoc}
   */
  public function prepareDirectory(&$directory, $options = self::MODIFY_PERMISSIONS) {
    // First we get the scheme.
    $scheme = $this->uriScheme($directory);

    // Depending on the scheme, we do different checks.
    switch ($scheme) {
      case 'public':
        // If our connection is wired, just return TRUE.
        if ($this->azureBlobFsService->publicAzureFileSystemIsEnabled()) {
          return TRUE;
        }
        break;

      case 'private':
        // If our connection is privately wired, just return TRUE.
        if ($this->azureBlobFsService->privateAzureFileSystemIsEnabled()) {
          return TRUE;
        }
        break;
    }


    // Otherwise, just do the default parent behavior.
    return parent::prepareDirectory($directory, $options);
  }

  /**
   * Get the real destination given a provided one.
   *
   * The idea here is that if the destination ends with a file extension, we
   * don't have to change anything, but if it doesn't we need to append the
   * source filename to it.
   *
   * @param string $source
   *   The source file.
   * @param string $destination
   *   The destination to alter.
   */
  private function determineRealDestination(string $source, string &$destination): void {
    $extension = pathinfo($destination, PATHINFO_EXTENSION);

    if (!$extension) {
      $destination = $this->streamWrapperManager->normalizeUri($destination . '/' . $this->basename($source));
    }
  }

}

